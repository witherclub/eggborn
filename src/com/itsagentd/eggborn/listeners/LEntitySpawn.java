package com.itsagentd.eggborn.listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.inventory.ItemStack;

import com.itsagentd.eggborn.EggBorn;

public class LEntitySpawn implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onCreatureSpawnEvent( CreatureSpawnEvent event) {
		
		if (event.getSpawnReason() == SpawnReason.BREEDING) {
			World world = event.getLocation().getWorld();
			double x = event.getLocation().getX();
			double y = event.getLocation().getY();
			double z = event.getLocation().getZ();
			
			short typeID = -1;
			
			switch (event.getEntity().getName()) {
			
			case "Chicken":
				if(EggBorn.config.getBoolean("drops.spawnegg.chicken"))
					typeID = EntityType.CHICKEN.getTypeId();
				break;
				
			case "Cow":
				if(EggBorn.config.getBoolean("drops.spawnegg.cow"))
					typeID = EntityType.COW.getTypeId();
				break;
				
			case "Horse":
				if(EggBorn.config.getBoolean("drops.spawnegg.horse"))
					typeID = EntityType.HORSE.getTypeId();
				break;
				
			case "Mooshroom":
				if(EggBorn.config.getBoolean("drops.spawnegg.mooshroom"))
					typeID = EntityType.MUSHROOM_COW.getTypeId();
				break;
				
			case "Ocelot":
				if(EggBorn.config.getBoolean("drops.spawnegg.ocelot"))
					typeID = EntityType.OCELOT.getTypeId();
				break;
				
			case "Pig":
				if(EggBorn.config.getBoolean("drops.spawnegg.pig"))
					typeID = EntityType.PIG.getTypeId();
				break;
				
			case "Rabbit":
				if(EggBorn.config.getBoolean("drops.spawnegg.rabbit"))
					typeID = EntityType.RABBIT.getTypeId();
				break;
				
			case "Sheep":
				if(EggBorn.config.getBoolean("drops.spawnegg.sheep"))
					typeID = EntityType.SHEEP.getTypeId();
				break;
				
			case "Villager":
				if(EggBorn.config.getBoolean("drops.spawnegg.villager"))
					typeID = EntityType.VILLAGER.getTypeId();
				break;
				
			case "Wolf":
				if(EggBorn.config.getBoolean("drops.spawnegg.wolf"))
					typeID = EntityType.WOLF.getTypeId();
				break;
			}
			
			if (typeID != -1) {
				
				event.getLocation().getWorld().dropItemNaturally(new Location(world, x, y, z), new ItemStack(Material.MONSTER_EGG, 1, typeID));
				event.setCancelled(true);
			}
		}
	}
}
