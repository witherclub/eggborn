package com.itsagentd.eggborn;

import java.util.logging.Logger;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.itsagentd.eggborn.listeners.LEntitySpawn;
import com.itsagentd.eggborn.util.ConfigFile;

public class EggBorn extends JavaPlugin {
	
	public final static Logger log = Logger.getLogger("Minecraft");
	public static FileConfiguration config;
	
	@Override
	public void onEnable() {
		ConfigFile configfile = new ConfigFile();
		configfile.loadConfig("config.yml", this);
		
		config = configfile.getConfig();
		
		getServer().getPluginManager().registerEvents(new LEntitySpawn(), this);
	}
	
	@Override
	public void onDisable() {
		
	}
}
